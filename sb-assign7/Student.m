//
//  Student.m
//  sb-assign7
//
//  Created by Student on 2014-10-20.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "Student.h"

@implementation Student

- (instancetype) initFirst:(NSString *)first Last:(NSString *)last
{
  if (self = [super init])
  {
    if (!(2 < first.length && first.length < 20))
    {
      _firstname = @"";
    }
    if (!(2 < last.length && last.length < 20))
    {
      _lastname = @"";
    }
  }
  return self;
}
- (NSMutableArray*) getCourse:(NSString*) course
{
  NSString* input = [course lowercaseString];
  if ([input compare:@"eng"] == NSOrderedSame)
  {
    return self.eng;
  }
  else if([input compare:@"cps"] == NSOrderedSame)
  {
    return self.cps;
  }
  else if([input compare:@"img"] == NSOrderedSame)
  {
    return self.img;
  }
  return 0;
}
-(NSString*) showGrades:(NSString*) course
{
  NSMutableString * ret = [[NSMutableString alloc]init];
  
  NSMutableArray* array = [self getCourse:course];

  for(NSNumber* num in array)
    [ret appendFormat:@"%@\n", num];
  return ret;
}
- (double) bestGrade: (NSString*) course
{
  NSMutableArray* array = [self getCourse:course];
  
  double best = 0;
  
  for (NSNumber* num in array)
  {
    double current = [num doubleValue];
    
    if(current > best)
      best = current;
  }
  return best;
}
- (double) worstGrade: (NSString*) course
{
  NSMutableArray* array = [self getCourse:course];
  
  double best = 100;
  
  for (NSNumber* num in array)
  {
    double current = [num doubleValue];
    
    if(current < best)
      best = current;
  }
  return best;
}
- (double) averageGrade: (NSString*) course
{
  NSMutableArray* array = [self getCourse:course];
  
  double average = 0;
  
  for (NSNumber* num in array)
  {
    average += [num doubleValue];
  }
  return average/[array count];
}

@end