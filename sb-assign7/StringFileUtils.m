//
//  StringFileUtils.m
//  sb-assign7
//
//  Created by Student on 2014-10-20.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "StringFileUtils.h"

@implementation StringFileUtils
{
  NSMutableArray * cps, * eng, * img;
  NSString * firstname;
  NSString * lastname;
}

- (NSArray *) getNSArrayOfLinesFromFolder:(NSString *)folderName andFilename:(NSString *)fileName
{
  NSError * error;

  NSString * filePath = [NSHomeDirectory() stringByAppendingPathComponent:
                         @"Desktop/sb-assign7/"];

  filePath = [filePath stringByAppendingString:@"/"];
  filePath = [filePath stringByAppendingString:folderName];
  filePath = [filePath stringByAppendingString:@"/"];
  filePath = [filePath stringByAppendingString:fileName];
  NSLog(@"The file path is : %@", filePath);

  NSString * fileContents = [NSString stringWithContentsOfFile:filePath
                                    encoding:NSUTF8StringEncoding error:&error];

  if (error)
  {
    NSLog(@"Error reading file: %@", error.localizedDescription);
  }

  NSArray * listArray = [fileContents componentsSeparatedByString:@"\n"];

  // maybe for debugging...
  // NSLog(@"contents: %@", fileContents);
  // NSLog(@"items = %lu", (unsigned long) [listArray count]);

  return listArray;
}

- (instancetype) initFromFile:(NSString *)filename
{
  if (self = [super init])
  {
    cps = [[NSMutableArray alloc] init];
    eng = [[NSMutableArray alloc] init];
    img = [[NSMutableArray alloc] init];
    firstname = @"";
    lastname = @"";
  }
  else
  {
    return nil;
  }

  NSArray * lines = [self getNSArrayOfLinesFromFolder:@"./datafiles"
                                          andFilename:filename];

  NSString * key, * value;

  NSCharacterSet * set = [NSCharacterSet
                          characterSetWithCharactersInString:@": "];

  for (NSString * str in lines)
  {
    NSScanner * scanner = [NSScanner scannerWithString:str];

    [scanner setCharactersToBeSkipped:set];

    [scanner scanUpToCharactersFromSet:set intoString:&key];

    [scanner scanUpToCharactersFromSet:set intoString:&value];

    [self setKey:key andValue:value];
  }

  return self;
}
BOOL validRange(double input)
{
  if (0 < input && input < 100)
  {
    return YES;
  }
  return NO;
}
- (void) setKey:(NSString *)key andValue:(NSString *)value
{
  NSString * lowerkey = [key lowercaseString];

  if ([lowerkey compare:@"first"] == NSOrderedSame)
  {
    firstname = value;
  }
  else if ([lowerkey compare:@"last"] == NSOrderedSame)
  {
    lastname = value;
  }
  else if (validRange([value doubleValue]))
  {
    if ([lowerkey compare:@"cps"] == NSOrderedSame)
    {
      [cps addObject:[NSNumber numberWithDouble:[value doubleValue]]];
    }
    else if ([lowerkey compare:@"eng"] == NSOrderedSame)
    {
      [eng addObject:[NSNumber numberWithDouble:[value doubleValue]]];
    }
    else if ([lowerkey compare:@"img"] == NSOrderedSame)
    {
      [img addObject:[NSNumber numberWithDouble:[value doubleValue]]];
    }
  }
}

- (Student *) createStudent
{
  Student * student = [[Student alloc] initFirst:firstname Last:lastname];

  student.cps = cps;
  student.eng = eng;
  student.img = img;
  return student;
}
@end