//
//  Student.h
//  sb-assign7
//
//  Created by Student on 2014-10-20.
//  Copyright (c) 2014 Student. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface Student : NSObject

@property NSString * firstname;
@property NSString * lastname;
// --------------------------
@property NSMutableArray * eng, * img, * cps;

// Should check that the first and last name are (2 < x < 20)
- (instancetype) initFirst:(NSString *)first Last:(NSString *)last;

// Returns a string to show grades with
- (NSString *) showGrades:(NSString *)section;

// Returns best worst and average grade
- (double) bestGrade: (NSString*)course;
- (double) worstGrade: (NSString*)course;
- (double) averageGrade:(NSString*)course;

@end
