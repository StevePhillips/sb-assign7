//
//  main.m
//  sb-assign7
//
//  Created by Student on 2014-10-20.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "StringFileUtils.h"
#import "ConsoleLine.h"
#include "Student.h"

int main(int argc, const char * argv[])
{
  @autoreleasepool
  {
    ConsoleLine * cl = [[ConsoleLine alloc] init];

    while (TRUE)
    {
      Student* student = NULL;
      
      printf("Please choose a command:\n1) Show Student\n2) Quit\n>");
      NSString* input = [cl getConsoleLine];
      
      switch ([input intValue]) {
        case 1:
        {
          printf("Which Student?\n1) Student 1\n2) Student 2\n3) Student 3\n>");
          input = [cl getConsoleLine];
          NSString* studString =
            [NSString stringWithFormat:@"student%i.txt",[input intValue]];
          
          StringFileUtils* utils = [[StringFileUtils alloc]
                                     initFromFile:studString];
          student = [utils createStudent];
          
          printf("Which course? eng, img or cps: ");
          
          input = [cl getConsoleLine];
          
          printf("Which action?\n1) Best grade\n2) Worst grade\n3) Average"
                 "grade\n4) List\n>");
          
          int choice = [[cl getConsoleLine] intValue];
          
          switch (choice) {
            case 1:
              NSLog(@"%f", [student bestGrade:input]);
              break;
            case 2:
              NSLog(@"%f", [student worstGrade:input]);
              break;
            case 3:
              NSLog(@"%f", [student averageGrade:input]);
              break;
            case 4:
              NSLog(@"%@", [student showGrades:input]);
              break;
          }
          break;
        }
        case 2:
          return 0;
      }
    }
  }
  return 0;
}
