//
//  StringFileUtils.h
//  sb-assign7
//
//  Created by Student on 2014-10-20.
//  Copyright (c) 2014 Student. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "Student.h"

@interface StringFileUtils : NSObject

- (instancetype) initFromFile:(NSString *)filename;
- (Student *) createStudent;
- (void) setKey:(NSString *)key andValue:(NSString *)value;

@end